#include "utils.h"

#include <cstdio>

void mpiDie(const char* filename, int line, int code)
{
    char buf[MPI_MAX_ERROR_STRING];
    int nchar;
    MPI_Error_string(code, buf, &nchar);

    fprintf(stderr, "%s:%d: MPI Error: %s", filename, line, buf);
    MPI_Abort(MPI_COMM_WORLD, -1);
}
