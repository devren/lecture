#pragma once

#include "celllists.h"
#include "definitions.h"
#include "particles.h"
#include "utils.h"

#include <array>
#include <mpi.h>
#include <vector>

class BasicExchanger
{
  public:
    BasicExchanger(MPI_Comm comm, const Domain& domain, int2 nRanks2D);

  protected:
    void send();
    void recv();

  protected:
    MPI_Comm comm;
    Domain domain;

    static constexpr int numNeighbors = 8;
    static constexpr int nRealPerParticle = sizeof(Particle) / sizeof(real);
    using BufferType = std::vector<Particle>;

    std::array<int, numNeighbors> dstRanks, srcRanks;

    std::array<BufferType, numNeighbors> sendBuffers, recvBuffers;
    std::array<int, numNeighbors> sendSizes, recvSizes;
    std::array<MPI_Request, numNeighbors> sendDataReqs, recvDataReqs;
    std::array<MPI_Request, numNeighbors> sendSizeReqs, recvSizeReqs;
};

class Redistributor : public BasicExchanger
{
  public:
    Redistributor(MPI_Comm comm, const Domain& domain, int2 nRanks2D);

    void redistribute(std::vector<Particle>& particles);

  private:
    void shiftAndPack(const std::vector<Particle>& particles);
    void unpack(std::vector<Particle>& particles);

    std::vector<Particle> bulk;
};

class GhostExchanger : public BasicExchanger
{
  public:
    GhostExchanger(MPI_Comm comm, const Domain& domain, int2 nRanks2D);

    void exchangeGhosts(const CellListsView& cl,
                        const std::vector<Particle>& particles,
                        std::vector<Particle>& ghosts);

  private:
    void shiftAndPack(const CellListsView& cl,
                      const std::vector<Particle>& particles);
    void unpack(std::vector<Particle>& ghosts);
};
