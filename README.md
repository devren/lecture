
# TA Contacts

* George (Head TA): <garampat@ethz.ch>
* Lucas:            <amlucas@ethz.ch>
* Martin:           <mboden@ethz.ch>
* Michalis:         <michaich@ethz.ch>
* Petr:             <kpetr@ethz.ch>
* Sergey:           <litvinov@ethz.ch>

# Exercise Hand-in

Please submit your exercise solutions via Moodle (nethz login required)

https://moodle-app2.let.ethz.ch/course/view.php?id=13666


# Frequently Asked Questions (FAQ)

Please have a look at these [Frequently asked questions](./faq/faq.md)
