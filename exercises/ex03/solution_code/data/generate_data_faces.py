from time import time
import logging

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import os

from sklearn.datasets import fetch_lfw_people

import numpy as np

print(__doc__)

# Display progress logs on stdout
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')


# #############################################################################
# Download the data, if not already on disk and load it as numpy arrays

lfw_people = fetch_lfw_people(min_faces_per_person=70, resize=0.4)


n_samples, h, w = lfw_people.images.shape

print("n_samples", n_samples)
print("h", h)
print("w", w)

data = lfw_people.data
data = data[:1280]
np.savetxt("faces_dataset.txt", data, delimiter=',', newline='\r\n')
del data

data = np.loadtxt("faces_dataset.txt", delimiter=',')
print(np.shape(data))

# im = np.reshape(data[0], (h, w))
# plt.imshow(im, cmap='gray', vmin=0, vmax=255)
# plt.show()

# data_mean = np.mean(data, axis=0)
# np.savetxt("faces_dataset_mean.txt", data_mean, delimiter=',', newline='\r\n')

# im = np.reshape(data_mean, (h, w))
# plt.imshow(im, cmap='gray', vmin=0, vmax=255)
# plt.show()


# data = data - data_mean
# DIMENSION (1288, 1850)
# data = data/255.0

# np.savetxt("faces_dataset.txt", data, delimiter=',', newline='\r\n')
# del data





